﻿namespace Match3Logic
{
    public class GameField
    {
        public const int MIN_SIZE = 3;
        public const int MAX_SIZE = 15;

        public static int ClampSize(int value)
        {
            if (value < MIN_SIZE)
                return MIN_SIZE;
            if (value > MAX_SIZE)
                return MAX_SIZE;
            return value;
        }
    }
}