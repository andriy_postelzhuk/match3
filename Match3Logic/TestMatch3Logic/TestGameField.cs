﻿using System;
using Match3Logic;
using NUnit.Framework;

namespace TestMatch3Logic
{
    [TestFixture]
    public class TestGameField
    {
        [Test]
        public void TestClampSize()
        {
            Assert.AreEqual(GameField.MIN_SIZE, GameField.ClampSize(0));
            Assert.AreEqual(GameField.MAX_SIZE, GameField.ClampSize(100000));
        }
    }
}